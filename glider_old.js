// This javascript function orchestrates the whole thing (creates wind)
$(function() {
	var plane = {};
	plane.test = "Test";


	var canvas = document.getElementById("glider_canvas");
	canvas.width = 2000;
	canvas.height = 1200;
	var fps = 27;

	var backgroundPosition = 0;

	var stage;
	var timestep = 20;
	var plane;
	var planeWidth = 235;
	var planeHeight = 160;
	var mousedown = false;

	var particleConatiner;

	var mouseX = 100;
	var mosueY = 100;

	// Parameters
	var particleCount = 2; // Particles created per tick
	var particleSize = 2;
	var particleSpeed = 18;
	var rnd = 9; // Multiplicitive element of randomness
	var windEffect = 0.17; // Multiplicitive impactof wind on plane

	var scrollSpeed = 5;

	var effectThreshold = 12; // Distance from center (horizontal) before wind is

	var focus = 'bias';  // Can be lateral, bias, fixed or plane


	var init = function()
	{
		canvas = document.getElementById("glider_canvas");
		stage = new createjs.Stage(canvas);
		//stage.scaleX = stage.scaleY = 0.2

		plane = new createjs.Bitmap("img/Plane.png");
		plane.x = 500;
		plane.y = 400;
		stage.addChild(plane);

		// Create container for all particles
		particleContainer = new createjs.Container();
		particleContainer.x = plane.x;
		particleContainer.y = 0;

		stage.addChild(particleContainer);

		stage.on("stagemousedown", function(e) {
			mousedown = true;
			mouseX = e.stageX;
			mouseY = e.stageY;

			stage.on("stagemousemove", function(e) {
				mouseX = e.stageX;
				mouseY = e.stageY;
			});
		});
		stage.on("stagemouseup", function() {
			mousedown = false;
			stage.removeAllEventListeners("stagemousemove");
		});

		

	    stage.update();

	    createjs.Ticker.setFPS(fps);
		createjs.Ticker.addEventListener("tick", updateCanvas);
	}

	function updateCanvas()
	{
		if (mousedown)
		{
			createParticles();
		}
		moveParticles();

		//Check Plane Angle
		if (plane.rotation > 30)
			plane.rotation = 30;
		else if (plane.rotation < -20)
			plane.rotation = -20;

		if (plane.y < 180)
			plane.y = 180;
		stage.update();

		backgroundPosition -= scrollSpeed;
		// Scroll Canvas
		//$(".container").css('backgroundPosition', backgroundPosition+"px center");
		$("#particle_count").html(particleContainer.getNumChildren());
	}

	function createParticles()
	{
		if (mouseX < plane.x + 1*planeWidth)
		{
			// We're too far to the left
			return false;
		}

		particle = new createjs.Shape();

		particle.graphics.beginFill("white").drawCircle(0, 0, particleSize+rnd*Math.random());

		var delta = 2*Math.random()-3;

		particle.x = mouseX-plane.x + delta;
		particle.y = mouseY + delta;
		particle.xspeed = particleSpeed + rnd*Math.random();

		if (focus === 'fixed')
			particle.yspeed = (600 - particle.y) / ( (particle.x - 0.4*planeWidth)  / particle.xspeed);
		else if (focus === 'plane')
			particle.yspeed = (plane.y - particle.y) / ( (particle.x - 0.4*planeWidth)  / particle.xspeed);
		else if (focus === 'lateral')
			particle.yspeed = rnd*0.5*Math.random() - rnd*Math.random();
		else if (focus === 'bias')
			particle.yspeed = (rnd*0.5*Math.random() - rnd*Math.random()) + (particle.yspeed = (600 - particle.y) / ( (particle.x - 0.4*planeWidth)  / particle.xspeed));
		particleContainer.addChild(particle);
	}

	function moveParticles()
	{
		for (var i = particleContainer.getNumChildren() - 1; i >= 0; i--) {
			var particle = particleContainer.getChildAt(i);

			if (particle.x < -200)
			{
				particleContainer.removeChildAt(i);
			} else {

				particle.xspeed -= (.15*Math.random());
				particle.alpha = particle.xspeed / (particleSpeed + 1.2*rnd);

				particle.x -= particle.xspeed;
				particle.y += particle.yspeed;

				if (particle.x < planeWidth  && particle.y < (plane.y + planeHeight ) && particle.y > plane.y )
				{
					planeCollision(particle);
				}
			}
		};
	}

	function planeCollision(particle)
	{
		//Upon collision - transfer some of wind's velocity to plane
		if (particle.yspeed < 0)
		{
			plane.y += windEffect * (particle.yspeed - particle.xspeed);
			//plane.rotation -= windEffect*4;
		}
		else
		{
			plane.y += windEffect * (particle.yspeed + particle.xspeed);
			//plane.rotation += windEffect*4;
		}
		particleContainer.removeChild(particle);
	}

	function floatPlane()
	{
		//plane.y += Math.sin()
	}


	init();
});