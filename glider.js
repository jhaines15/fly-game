// This javascript function orchestrates the whole thing (creates wind)
$(function() {
	var canvas = document.getElementById("glider_canvas");
	canvas.width = 2000;
	canvas.height = 1200;
	var fps = 25;

	var backgroundPosition = 0;

	var stage;
	var plane;
	var planeWidth = 235;
	var planeHeight = 160;
	var mousedown = false;

	var particleConatiner;

	var mouseX = 100;
	var mosueY = 100;

	// Parameters
	var particleCount = 2; // Particles created per tick
	var particleSize = 2;
	var particleSpeed = 18;
	var rnd = 9; // Multiplicitive element of randomness
	var windEffect = 0.09; // Multiplicitive impact of wind on plane

	var scrollSpeed = 2;

	var effectThreshold = 12; // Distance from center (horizontal) before wind is

	var focus = 'bias';  // Can be lateral, bias, fixed or plane

	var physics = FlyPhysics(1/fps, {x : 100, y: 1}, {x : -3.7, y: 0});

	var gameOver = false;
	var crashing = false;
	var gamePaused = true;

	var stability = 100;
	


	var init = function()
	{
		canvas = document.getElementById("glider_canvas");
		stage = new createjs.Stage(canvas);
		stage.y = -200;
		//stage.scaleX = stage.scaleY = 0.2

		plane = new createjs.Bitmap("img/Plane.png");
		plane.x = 500;
		plane.y = 600;
		stage.addChild(plane);
		physics.setPlane("plane1", plane);

		// Create container for all particles
		particleContainer = new createjs.Container();
		particleContainer.y = -200;
		particleContainer.x = plane.x;
		particleContainer.y = 0;

		stage.addChild(particleContainer);

		stage.on("stagemousedown", function(e) {
			mousedown = true;
			mouseX = e.stageX;
			mouseY = e.stageY;

			stage.on("stagemousemove", function(e) {
				mouseX = e.stageX;
				mouseY = e.stageY;
			});
		});
		stage.on("stagemouseup", function() {
			mousedown = false;
			stage.removeAllEventListeners("stagemousemove");
		});

		$(".overlay").hide();
		$("#btn_play").click(function() {
			gamePaused = false;
		});
		

	    stage.update();

	    createjs.Ticker.setFPS(fps);
		createjs.Ticker.addEventListener("tick", updateCanvas);
	}

	function updateCanvas()
	{
		var currentPlane = physics.getPlane();
		if (gamePaused)
		{
			// Don't update if paused
			$(".pause").show();
			return false;
		}
		$(".pause").hide();

		if (gameOver)
		{
			$(".game-over").show();
			return false;
		}

		if (mousedown)
		{
			createParticles();
		}
		moveParticles();

		//Check Plane Angle
		if (plane.rotation > 30)
			plane.rotation = 30;
		else if (plane.rotation < -20)
			plane.rotation = -20;

		if (plane.y < 180)
			plane.y = 180;
		stage.update();

		backgroundPosition -= scrollSpeed;
		
		//updateStabilityAltitude(stability, 0);
		
		//console.log(physics.getScrollSpeed());
		scrollSpeed = physics.getScrollSpeed();
		//scrollSpeed = stability;


		var dist = Math.round(currentPlane.position.x)-100;
		
		if (dist === 25)
		{
			$(".container").removeClass("day").addClass("afternoon");
		}
		else if (dist === 50)
		{
			$(".container").removeClass("afternoon").addClass("night");
		}

		// Scroll Canvas
		$(".background-3").css("left", "-="+(scrollSpeed*(6)).toString()+"px");
		$(".background-2").css("left", "-="+(scrollSpeed*(2.8)).toString()+"px");
		$(".background-1").css("left", "-="+(1.9).toString()+"px");


		if ($(".background-1").position().left < -14000)
			$(".background-1").css("left", "0px");
		if ($(".background-2").position().left < -8800)
			$(".background-2").css("left", "1400px");
		if ($(".background-3").position().left < -8800)
			$(".background-3").css("left", "1400px");

		$("#particle_count").html(particleContainer.getNumChildren());
		$("#distance").html(dist.toString());
	}

	function createParticles()
	{
		if (mouseX < plane.x + 1*planeWidth)
		{
			// We're too far to the left
			return false;
		}

		particle = new createjs.Shape();

		particle.graphics.beginFill("white").drawCircle(0, 0, particleSize+rnd*Math.random());

		var delta = 2*Math.random()-3;

		particle.x = mouseX-plane.x + delta;
		particle.y = mouseY + delta+200;
		particle.xspeed = particleSpeed + 2;

		if (focus === 'fixed')
			particle.yspeed = (600 - particle.y) / ( (particle.x - 0.4*planeWidth)  / particle.xspeed);
		else if (focus === 'plane')
			particle.yspeed = (plane.y - particle.y) / ( (particle.x - 0.4*planeWidth)  / particle.xspeed);
		else if (focus === 'lateral')
			particle.yspeed = rnd*0.5*Math.random() - rnd*Math.random();
		else if (focus === 'bias')
			particle.yspeed = 2 + (plane.y + 50 - particle.y) / ( (particle.x - 0.4*planeWidth)  / particle.xspeed);
		particleContainer.addChild(particle);
	}

	function moveParticles()
	{
		var windSum = {
			x : 0,
			y : 0
		}

		for (var i = particleContainer.getNumChildren() - 1; i >= 0; i--) {
			var particle = particleContainer.getChildAt(i);

			if (particle.x < -200)
			{
				particleContainer.removeChildAt(i);
			} else {

				particle.xspeed -= (.15*Math.random());
				particle.alpha = particle.xspeed / (particleSpeed + 1.2*rnd);

				particle.x -= particle.xspeed;
				particle.y += particle.yspeed;

				if (particle.x < planeWidth  && particle.y < (plane.y + planeHeight ) && particle.y > plane.y )
				{
					windSum.x -= particle.xspeed;
					windSum.y -= particle.yspeed;
					particleContainer.removeChild(particle);
				}
			}
		};

		windSum.x *= windEffect;
		windSum.y *= (windEffect*6);

		floatPlane(windSum);
		// console.log("Would be: ");
		// console.log(windSum);
	}

	function planeCollision(particle)
	{
		//Upon collision - transfer some of wind's velocity to plane
		if (particle.yspeed < 0)
		{
			plane.y += windEffect * (particle.yspeed - particle.xspeed);
			//plane.rotation -= windEffect*4;
		}
		else
		{
			plane.y += windEffect * (particle.yspeed + particle.xspeed);
			//plane.rotation += windEffect*4;
		}
		particleContainer.removeChild(particle);
	}

	// updates the plane with respect to calculated wind speed
	function floatPlane(windSum)
	{
		var userWind;
		if (windSum.y != 0)
			userWind = true;
		else
			userWind = false;

		stability = 100 - Math.round(physics.updatePlane(windSum.x, windSum.y, userWind));

		if (stability < 0)
			gameOver = true;
	}


	init();
});