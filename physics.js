//Constants
var m_sheet = 0.0045;
var W_sheet = 0.01;

var consoleCounter = 0;

// Parent plane class
function Plane(name) {
	this.name = name;
	this.mass = 20;
	this.img = "plane1.png";
	this.liftCoefficient = .7;
	this.crashed = false;

	this.position = {};
	this.velocity = {
		x : 1,
		y : 1
	};

	var bitmap;

	// Set the bitmap to a createJS bitmap object
	this.setBitmap = function(inBitmap)
	{
		this.bitmap = inBitmap;
	}
}

function Plane1(name)
{
	var plane = new Plane(name);

	plane.cp = 0.107109;
	plane.cm = 0.08;
	plane.S = .007346;
	plane.b = 13.8;
	plane.c = 10.7;
	plane.AR = plane.b/plane.c;

	plane.I = m_sheet * plane.S;

	plane.m = m_sheet;

	plane.velocityCap = 10;

	return plane;

}

var createHanger = function() {
	var plane1 = new Plane1("Plane 1");
	

	return {
		"plane1" : plane1
	}
}

// Library of physics and planes for Fly App
var FlyPhysics = function(timestep, initPosition, initVelocity) {
	var hanger = createHanger();
	var plane;

	// Constants
	var rho = 1.225; //Density of air 
	var g = -9.81; //Gravity
	// var CL = 0.7;
	var CDO = .07;
	var e = 0.7;
	var dt = timestep;

	var planeWind = {
		x : 0,
		y : 0
	}

	var scrollSpeed = 10;

	return {
		getScrollSpeed : function()
		{
			return scrollSpeed;
		},
		getPlane : function()
		{
			return plane;
		},
		// Takes integer argument for plane, sets the plane on FlyPhysics Object
		setPlane : function(planeName, bitmap)
		{
			plane = hanger[planeName];
			plane.position = initPosition;

			//plane.velocity = initVelocity;
			planeWind = initVelocity;

			plane.setBitmap(bitmap);
			plane.bitmap.x = plane.position.x;
			plane.bitmap.y = plane.position.y;
		},

		// Updates position, velocity of plane
		// Returns stability (higher is better)
		updatePlane : function(windX, windY, userWind)
		{
			var print = true;
			if (isNaN(plane.velocity.x) || isNaN(plane.velocity.y))
			{
				print = false;
				return false;
			}
			planeWind.x += (windX/21);
			planeWind.y -= (windY/9);

			plane.velocity.x = -1*planeWind.x;
			plane.velocity.y = -1*planeWind.y;

			 			// windX *= windModifier;
			// windY *= windModifier;
			// Define a few local vairablel
			var windMag = Math.sqrt(plane.velocity.x*plane.velocity.x + plane.velocity.y*plane.velocity.y);
			var alpha = Math.atan(plane.velocity.y/plane.velocity.x);
			
			var CL = 0.7;// + 2*Math.PI*(alpha+alpha_plane);
			
			//console.log(alpha);

			var CD = CDO + (CL*CL) / (Math.PI*e*plane.AR);

			var n_e_x = -1*Math.sin(alpha); 
			var n_e_y = Math.cos(alpha);

			var p_e_x = plane.velocity.x/windMag; 
			var p_e_y = plane.velocity.y/windMag;

			var weight = {
				x : 0,
				y : plane.m*g
			}

			var L = {
				x : 0.5*rho*Math.pow(windMag,2)*plane.S*CL*n_e_x,
				y : 0.5*rho*Math.pow(windMag,2)*plane.S*CL*n_e_y
			}

			//console.log(windMag);
			if (userWind)
			{
				var D = {
					x : 0,
					y : 0
				}
			} 
			else
			{
				var D = {
					x : -0.5*rho*Math.pow(windMag,2)*plane.S*CD*p_e_x,
					y : -0.5*rho*Math.pow(windMag,2)*plane.S*CD*p_e_y
				}
			}
			
			

			plane.position.x += plane.velocity.x * dt;
			plane.position.y += plane.velocity.y * dt;

			

			var a = {
				x : (L.x+D.x)/(3.*plane.m),
				y : (L.y+weight.y+D.y)/(3.*plane.m) 	// maybe check direction of D components
			}

			//console.log(a);

			plane.velocity = {
				x : plane.velocity.x + (a.x*dt),
				y : plane.velocity.y + (a.y*dt)
			}

			if (true) //(consoleCounter < 2) //(consoleCounter < 8)
			{
				// console.log("Velocity:");
			 // 	console.log(plane.velocity);

				// // console.log("Wind: ");
				// // console.log(windX +", "+windY);
				// // console.log(relVelocity);
				// console.log("alphas");
				// console.log(alpha);
				// // // console.log("CL");
				// // // console.log(CL);
				// console.log("Lift");
				// console.log(L);
				// // console.log("Weight");
				// // console.log(weight.y);

				// console.log("Drag: ");
				// console.log(D);

				// // console.log("Accel: ");
				// // console.log(a);

				// // console.log("Position:");
				// // console.log(plane.position.y);

				// //console.log(a);
			}
			consoleCounter++;

			scrollSpeed = 2 + Math.abs(plane.velocity.x + windX)/2;

			planeWind.x = -plane.velocity.x;
			planeWind.y = -plane.velocity.y;

			if (plane.velocity.x < 0)
				plane.velocity.x = 0;

			//console.log(plane.velocity);

			//Bound plane position by 11, -7
			
			 //console.log("-------------------");
			
			plane.bitmap.y = 600 - (plane.position.y*40);

			if (plane.bitmap.y > 1300 || plane.bitmap.y < 0)
				return 1000;

			return Math.sqrt(a.x*a.x + a.y*a.y);
		}
	}
}


